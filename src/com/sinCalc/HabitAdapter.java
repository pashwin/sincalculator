package com.sinCalc;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseUtils;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class HabitAdapter extends BaseAdapter {
	
	private LayoutInflater mInflater;

	public HabitAdapter(Context context) {
		mInflater = LayoutInflater.from(context);

	}

	private static final String[] country = { "Iceland", "India", "Indonesia",

	"Iran", "Iraq", "Ireland", "Israel", "Italy", "Laos", "Latvia",

	"Lebanon", "Lesotho ", "Liberia", "Libya", "Lithuania",

	"Luxembourg" };

	private static final String[] curr = { "ISK", "INR", "IDR", "IRR", "IQD",

	"EUR", "ILS", "EUR", "LAK", "LVL", "LBP", "LSL ", "LRD", "LYD",

	"LTL ", "EUR" };

	@Override
	public int getCount() {
		return country.length;
	}

	@Override
	public Object getItem(int arg0) {
		return country[arg0];
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	
	static class ViewHolder {
		
		 TextView text;
		
		 TextView text2;
		 
		 TextView text3;
		
		 }


	@Override
	public View getView(int arg0, View convertView, ViewGroup arg2) {
		ViewHolder holder;

		
		
		
		
		readDatabase();
		
		
		
		
		
		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.textview, null);
			

			holder = new ViewHolder();

			Button b1 = (Button)convertView.findViewById(R.id.button1);
			b1.setGravity(Gravity.RIGHT);
//			b1.setBackgroundColor(Color.RED);
			Button b2 = (Button)convertView.findViewById(R.id.button2);
//			b2.setBackgroundColor(Color.BLUE);
			
			holder.text = (TextView) convertView.findViewById(R.id.TextView01);
//
			holder.text2 = (TextView) convertView.findViewById(R.id.TextView02);
			
			holder.text3 = (TextView) convertView.findViewById(R.id.TextView03);
//
			convertView.setTag(holder);

		} else {

			holder = (ViewHolder) convertView.getTag();

		}

		holder.text.setText(curr[arg0]+"1");
//
		holder.text2.setText(country[arg0]+"2");
		
		holder.text3.setText(country[arg0]+"33");

		return convertView;

	}

	private void readDatabase() {
		DBAdapter dbAdapter = DBAdapter.getDBAdapterInstance(new SinCalc());
		dbAdapter.openDataBase();
		 
		ContentValues initialValues = new ContentValues();
		ArrayList selectValues;
		
		try{
			selectValues =dbAdapter.selectRecordsFromDBList("select * from habits", null);
//			ArrayList selectValues =dbAdapter.selectRecordsFromDBList("select * from habits", new String[]{"habit", "month", "target","achieved"});	
		}catch(Exception e){
			e.printStackTrace();
		}
		

		System.out.println("ASHWIN");
//		Toast.makeText(Update.this, n+" rows updated", Toast.LENGTH_SHORT).show();
		
	}
	
	

}
