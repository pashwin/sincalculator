package com.sinCalc;

import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SinCalc extends Activity {

	private ListView lv1;

	private String lv_arr[] = { "Smoking", "Drinking", "Family",
			"Dov" };

	@Override
	public void onCreate(Bundle icicle)

	{

		
		
		createDatabase();
		super.onCreate(icicle);

		setContentView(R.layout.main);

		lv1 = (ListView) findViewById(R.id.ListView01);
		
		
		
		
		//	
		// // By using setAdpater method in listview we an add string array in
		// list.
		//	
		lv1.setAdapter(new HabitAdapter(this));

	}

	private void createDatabase() {
		
		DBAdapter dbAdapter=DBAdapter.getDBAdapterInstance(this);
	      try {
	          dbAdapter.createDataBase();
	      } catch (IOException e) {
	          System.out.println(""+e.getMessage());
	      }

		
	}

}
