package com.sinCalc.habit;

public class SmokingHabit implements Habit {
	
	
	private int target = 10;
	
	private int alreadySmoked = 1;
	
	
	
	

	public int getTarget() {
		return target;
	}

	public void setTarget(int target) {
		this.target = target;
	}

	public int getAlreadySmoked() {
		return alreadySmoked;
	}

	public void setAlreadySmoked(int alreadySmoked) {
		this.alreadySmoked = alreadySmoked;
	}

}
